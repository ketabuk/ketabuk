<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/volunteer', function () {
    return view('volunteer');
})->name('volunteer');

Auth::routes(['verify' => true]);



Route::get('/home', 'HomeController@index')->name('home');



Route::get('users/{id}', 'UserController@show');



Route::get('/books', 'BookController@index');
Route::post('/books', 'BookController@store');

// must be placed above /books/{id} so that
// create wouldn't be condidered the id
Route::get('/books/create', 'BookController@create')->middleware('verified');

Route::get('/books/{id}', 'BookController@show');
Route::get('/books/{id}/delete', 'BookController@delete');
Route::get('/books/{id}/holders', 'BookController@holders');
Route::put('/books/{id}/available', 'BookController@set_available')->middleware('verified');
Route::put('/books/{id}/unavailable', 'BookController@set_unavailable')->middleware('verified');



Route::get('/transfer/{id}', 'BookController@transfer_view')->middleware('verified');
Route::post('/transfer', 'BookController@transfer')->middleware('verified');



Route::get('/conversations', 'ConversationController@index')->middleware('verified');
Route::get('/conversations/{id}', 'ConversationController@conversation')->middleware('verified');
Route::post('/conversations/{id}', 'ConversationController@store')->middleware('verified');