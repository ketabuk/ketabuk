<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MessageReceived extends Notification
{
    use Queueable;

    private $from;
    private $body;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $from, String $body)
    {
        $this->from = $from;
        $this->body = $body;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Message on ketabuk.org!')
                    ->greeting('Hi,')
                    ->line("You've received a new message from " . $this->from->name . "!")
                    ->line('The message reads:')
                    ->line('"' . $this->body . '"')
                    ->action("Reply to Message", url('/conversations/' . $this->from->id))
                    ->line('We love you :D <3');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "from" => $this->from->id,
            "body" => $this->body
        ];
    }
}
