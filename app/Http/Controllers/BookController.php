<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Imagick;
use App\User;
use App\Book;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', ['books' => Book::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('create', [ 'categories' => $categories ]);
    }

    public function delete($id)
    {
        if(Auth::id() != 1)
        {
            return response(403);
        }
        
        DB::delete('delete from book_user where book_id = ?',[$id]);
        DB::delete('delete from books where id = ?',[$id]);
        return redirect('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'author' => 'required|max:255',
            'translator' => 'max:255',
            'image' => 'required|image|dimensions:min_width=512,min_height=512',
            'category' => 'required',
        ])->validate();

        $book = new Book;
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $category = Category::find($request->input('category'));
        $book->category_id = $category->id;
        $book->available = true;
        $book->original_owner = Auth::user()->id;
        $book->current_holder = Auth::user()->id;

        if($request->input('translator') != "")
        {
            $book->translator = $request->input('translator');
        }

        $image_path = $request->file('image')->store('public/book_images/');
        $image_path = '/storage/book_images/' . basename($image_path);
        $book->image_path = $image_path;

        $image = new Imagick('./' . $image_path);
        $image->thumbnailImage(512, 0);
        $image->writeImage();

        $book->save();
        $book->past_holders()->save(Auth::user());

        return redirect('/books/' . $book->id);
    }

    public function holders($id)
    {
        $book = Book::findOrFail($id);
        $past_holders = $book->past_holders;
        return view('holders', ['book' => $book, 'holders' => $past_holders]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('book', ['book' => Book::find($id)]);
    }

    public function set_available($id)
    {
        $book = Book::findOrFail($id);

        if($book->current_holder != Auth::user()->id)
        {
            return response(403);
        }

        $book->available = true;
        $book->save();
        return back();
    }

    public function set_unavailable($id)
    {
        $book = Book::findOrFail($id);

        if($book->current_holder != Auth::user()->id)
        {
            return response(403);
        }
        
        $book->available = false;
        $book->save();
        return back();
    }

    public function transfer_view($id)
    {
        $user = User::findOrFail($id);
        return view('transfer', ['user' => $user]);
    }

    public function transfer(Request $request)
    {
        Validator::make($request->all(), [
            'book' => 'required',
            'user' => 'required'
        ])->validate();

        $user_id = $request->input('user');
        $book_id = $request->input('book');

        $book = Book::find($book_id);
        $user = User::find($user_id);

        $me = Auth::user();

        if($book->current_holder != Auth::user()->id)
        {
            return response(403);
        }

        $book->current_holder = $user->id;
        $book->available = false;
        $book->save();
        $book->past_holders()->save($user);

        $message = "You have successfully transfered the book!";
        return redirect('/books/' . $book->id)->with('status', $message);
    }
}
