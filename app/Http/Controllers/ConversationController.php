<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Message;
use App\Conversation;
use App\Notifications\MessageReceived;

use Illuminate\Http\Request;

class ConversationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$conversations = Auth::user()->conversations;

    	$users = collect([]);
        $users_state = collect([]);

    	foreach ($conversations as $conversation)
    	{
    		$all_users = $conversation->users->all();

    		$user1 = $all_users[0];
    		$user2 = $all_users[1];

    		if(Auth::user()->id == $user1->id)
    		{
    			$users->push($user2);
    		}
    		else
    		{
    			$users->push($user1);
    		}

            $has_notification = false;

            foreach (Auth::user()->unreadNotifications as $notification)
            {
                if($notification->data['from'] == $users->last()->id)
                {
                    $users_state->push(1);
                    $has_notification = true;
                    break;
                }
            }

            if($has_notification == false)
            {
                $users_state->push(0);
            }
    	}

        assert($users->count() == $users_state->count());

        return view('conversations', [ 'users' => $users, 'users_state' => $users_state ] );
    }

    public function conversation($id)
    {
    	$conversation = Auth::user()->conversations->filter(
    		function ($value, $key) use($id) {
    			$users = $value->users->all();
    			$user1 = $users[0];
    			$user2 = $users[1];

    			if($user1->id == Auth::user()->id)
    			{
    				return $user2->id == $id;
    			}
    			else if($user2->id == Auth::user()->id)
    			{
    				return $user1->id == $id;
    			}
			    
			    throw "wtf";
			})->first();

        $other_user = User::find($id);

    	if($conversation == null)
    	{
    		$conversation = new Conversation;
    		$conversation->save();
    		$conversation->users()->saveMany([Auth::user(), $other_user]);
    	}

        foreach (Auth::user()->unreadNotifications as $notification)
        {
            if($notification->data['from'] == $other_user->id)
            {
                $notification->markAsRead();
            }
        }

        return view('conversation',
            [
                'conversation' => $conversation,
                'other_user'   => $other_user
            ]);
    }

    private function get_other_user($conv)
    {
        $users = $conv->users->all();
        $user1 = $users[0];
        $user2 = $users[1];
        
        if($user1->id == Auth::user()->id)
        {
            return $user2;
        }
        else if($user2->id == Auth::user()->id)
        {
            return $user1;
        }
    }

    public function store(Request $request)
    {
        $conv = Conversation::find($request->input('conv_id'));
        $other_user = $this->get_other_user($conv);
        $m = new Message;
        $m->user_id = Auth::user()->id;
        $m->body = $request->input('body');
        $conv->messages()->save($m);
        $other_user->notify(new MessageReceived(Auth::user(), $m->body));
        return redirect('/conversations/' . $other_user->id);
    }
}
