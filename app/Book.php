<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function holder()
    {
    	return $this->belongsTo('App\User', 'current_holder');
    }

    public function owner()
    {
    	return $this->belongsTo('App\User', 'original_owner');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function past_holders()
    {
        return $this->belongsToMany('App\User');
    }
}
