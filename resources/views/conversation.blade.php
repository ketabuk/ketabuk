@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="alert alert-primary" role="alert">
  If you decide to meet this person, please do so in a public place and at day time.
  There are bad people on the internet that can hurt you.
  Take care of yourself :)
</div>
<div class="card-header">You are talking to {{ $other_user->name }}</div>
<div class="card-body">
<table class="table">
    <tbody>
        @foreach ($conversation->messages as $message)
        <tr>
            <th scope="row"></th>
            <td>
            <a href="/users/{{ $message->user->id }}">
            {{ $message->user->name }}
            </a>
            </td>
            <td>
            {{ $message->body }}
            </td>
        </tr>
        @endforeach

        <th scope="row"></th>
        <td>

        </td>
    </tr>
    </tbody>
</table>
<form method="POST" action="/conversations/{{ $other_user->id }}">
@csrf
    <input id="body" name="body" class="form-control rounded-0" autofocus></textarea>
    <input type="hidden" id="conv_id" name="conv_id" value="{{ $conversation->id }}"/>
    <br/>
    <button type="submit" class="btn btn-primary">Send</button>
</form>
</div>
</div>
</div>
</div>
</div>
@endsection
