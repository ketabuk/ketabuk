@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header"><h2>{{ $book->title }}</h2></div>

<div class="card-body">

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if( $book->image_path == null)
  <?php
    $r = rand(1, 8);
    $book->image_path = '/images/no_image' . strval($r) . '.png';
  ?>
@endif

<img style="float:left; margin:30px" width="300" src="{{ $book->image_path }}"/>

<div style="font-size: 45px">
  The Three Holy Rules
</div>
<div align="left">
  @include('rules')
</div>

<br/>
<br/>

<strong>By {{ $book->author }}</strong>
@if($book->translator != null)
    <br/>
    <strong>Translated by {{ $book->translator }}</strong>
@endif

<br/>
<br/>
<strong>Genre:</strong> {{ $book->category->name }}
<br/>
<br/>
<a href="/users/{{ $book->holder->id }}"><strong>{{ $book->holder->name }}</strong></a> currently has this book.
<br/>
<br/>
@auth
@if($book->available)
This book is <span style="color:green">available!</span>
<br/>
<br/>
  @if($book->current_holder != Auth::user()->id)
    Want to read this book?
    <br/>
    <a class="btn btn-primary" href="/conversations/{{ $book->holder->id }}">
    Send {{ explode(' ', $book->holder->name)[0] }} a message
    </a>.
  @else
    <form action="/books/{{ $book->id }}/unavailable" method="POST">
    @csrf
    @method('PUT')
  	  <button class="btn btn-primary" href="/conversations/{{ $book->holder->id }}">
  	  Set this book to unavailable
  	  </button>
    </form>
  @endif
@else
The book is currently <span style="color:red">unavailable</span>.
  @if($book->current_holder != Auth::user()->id)
  {{ explode(' ', $book->holder->name)[0] }} will mark it as available once they're done reading.
  @else
  <br/>
  <br/>
  <form action="/books/{{ $book->id }}/available" method="POST">
  @csrf
  @method('PUT')
  	<button class="btn btn-primary" href="/conversations/{{ $book->holder->id }}">
  	Set this book to available
  	</button>
  </form>
  @endif
@endif
@endauth

@guest
@if($book->available)

This book is <span style="color:green">available!</span>
<br/>
<br/>
Want to read this book?
<br/>
<a class="btn btn-primary" href="/conversations/{{ $book->holder->id }}">
Send {{ explode(' ', $book->holder->name)[0] }} a message
</a>.

@else

The book is currently <span style="color:red">unavailable</span>.
{{ explode(' ', $book->holder->name)[0] }} will mark it as available once they're done reading.

@endif

@endguest
<br/>
<br/>
Thanks to <a href="/users/{{ $book->owner->id }}"><strong>{{ $book->owner->name }}</strong></a> who has graciously donated this copy of the book.
<br/>
<br/>
<p>Wanna see who previously <a href="/books/{{ $book->id }}/holders">held this book</a>?</p>
</div>
</div>
</div>
</div>
</div>
@endsection
