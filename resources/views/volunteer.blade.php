@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header">Volunteer</div>

<div class="card-body">

<h3>So you wanna to help? Great!</h3>
<ul>
    <li>If the website is up, please donate a book or request one and read it :D</li>
    <li>Tell your friends about it</li>
    <li>Tell your parents about it</li>
    <li>Evangalise so hard about how this is the truth we've all been looking for. Tell them how blind we've been
all this time. JK be cool, Don't be that guy</li>
</ul>

<h3>Help with the development?</h3>
I organize the work on this <a href="https://trello.com/b/grxbK9HC/development">Trello Board</a>.
Contact me and I'll assign you a task. Here's my email: adhamzahranfms@gmail.com
</div>
</div>
</div>
</div>
</div>
@endsection
