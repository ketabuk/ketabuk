@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header">Conversations</div>

<div class="card-body">
<table class="table">
    <tbody>
        @for($i = 0; $i < $users->count(); $i++)
            <tr>
            <th scope="row"></th>

            <td>

            <a href="/conversations/{{ $users[$i]->id }}">
            @if($users_state[$i] == 1)
            <strong>
                <span style="color:red">*</span> 
            {{ $users[$i]->name }}
            </strong>
            @else
            {{ $users[$i]->name }}
            @endif
            </a>
            </td>
        @endfor
    </tr>
    </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
@endsection
