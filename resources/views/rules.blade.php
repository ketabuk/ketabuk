<ol>
    <li>Donate books because you're awesome (You don't have to).</li>
    <li>When you're done reading. You have to forward the book to someone else.</li>
    <li>If 3 weeks have passed and you haven't been actively reading the book, you have to forward it to someone else.</li>
</ol>