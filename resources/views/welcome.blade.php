<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ketabuk</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #313537;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                margin: 10px;
                padding: 20px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                flex-direction: column; 
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .little_title {
                font-size: 45px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 40px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-width">
            <div class="title m-b-md">
                <img src="images/logo.jpg" width="256"/>
            </div>

            <div class="little_title">
                Take books for free. Give books for free.
                <br/>
                <br/>
            </div>

            @auth
            <div class="little_title">
                Hi {{ explode(' ', Auth::user()->name)[0] }}!
                <br/>
                <br/>
            </div>
            @endauth

            <div class="links">
                    <a href="{{ route('home') }}">Browse Books</a>
                    <a href="{{ route('volunteer') }}">Volunteer</a>
                @guest
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                @endguest

                    <br/>
                    <br/>
            </div>

            <br/>
            <br/>

            <img src="/images/exchange.png" class="img-fluid" style="width:100%; max-width: 846px"/>

            <br/>
            <br/>

            <div class="little_title">
                The Three Holy Rules
            </div>

            <div align="left">
                @include('rules')
            </div>
        </div>
    </body>
</html>
