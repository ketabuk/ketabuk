@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header">People who held <a href="/books/{{ $book->id }}">{{ $book->title }}</a></div>

<div class="card-body">
<table class="table">
    <tbody>
        @foreach ($holders as $user)
            <tr>
            <th scope="row"></th>

            <td>

            <a href="/users/{{ $user->id }}">
            
            {{ $user->name }}

            </a>
            </td>
        @endforeach
    </tr>
    </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
@endsection
