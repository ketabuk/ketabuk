@extends('layouts.app')

@section('content')

<?php 
$i = 0;
?>

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header">Books</div>
<a href="/books/create" class="btn btn-success">Donate a book to ketabuk <3</a>
<div class="card-body">

<div class="row">
@foreach ($books as $book)
<div class="card col-sm-4">
<div class="card-body">
    @if( $book->image_path == null)
        <?php
            $r = rand(1, 8);
            $book->image_path = '/images/no_image' . strval($r) . '.png';
        ?>
    @endif

    <a href="/books/{{$book->id}}">
        <img class="card-img-top" src="{{ $book->image_path }}"/>
        <h5 class="card-title" style="color:black">{{ $book->title }}</h5>
        @if($book->available)
        <span style="color:green">Available</span>
        @else
        <span style="color:red">Unavailable</span>
        @endif
    </a>
</div>
</div>
@endforeach
</div>

</div>
</div>
</div>
</div>
</div>
@endsection
