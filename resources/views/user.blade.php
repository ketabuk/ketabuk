@extends('layouts.app')

@section('content')

<?php 
$i = 0;
?>

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header"><h2>{{ $user->name }}</h2></div>

@auth
@if($user->id == Auth::user()->id)
<a href="/books/create" class="btn btn-success">Donate a book to Ketabuk <3</a>
@endif
@endauth

<div class="card-body">

@auth
@if($user->id != Auth::user()->id)

<a href="/transfer/{{ $user->id }}" class="btn btn-success">
Transfer a book to {{ $user->name }}
</a>

<br/>
<br/>
@endif
@endauth

@if($user->books_held->count() > 0)
<strong>Books I hold:</strong>
<br/>
<br/>
<table class="table">
<tbody>
        @foreach ($user->books_held as $book)
            @if($i == 0)
                <tr>
                <th scope="row"></th>
            @endif

            <td>

            @if( $book->image_path == null)
                <?php
                    $r = rand(1, 8);
                    $book->image_path = '/images/no_image' . strval($r) . '.png';
                ?>
            @endif

            <a href="/books/{{$book->id}}">
                <img width="300" src="{{ $book->image_path }}"/>
                <br/>
                {{ $book->title }}
                <br/>
                @if($book->available)
                <span style="color:green">Available</span>
                @else
                <span style="color:red">Unavailable</span>
                @endif
            </a>
            </td>
            <?php $i++; ?>

            @if($i == 3)
                </tr>
                <?php $i = 0; ?>
            @endif
        @endforeach
</tbody>
</table>
@else
<strong>I don't currently hold any books from Ketabuk. Hopefully I will soon :D</strong>
<br/>
<br/>
@endif

<?php 
$i = 0;
?>

@if($user->books_owned->count() > 0)
<strong>Books I Donated to Ketabuk:</strong>
<br/>
<br/>
<table class="table">
<tbody>
        @foreach ($user->books_owned as $book)
            @if($i == 0)
                <tr>
                <th scope="row"></th>
            @endif

            <td>

            @if( $book->image_path == null)
                <?php
                    $r = rand(1, 8);
                    $book->image_path = '/images/no_image' . strval($r) . '.png';
                ?>
            @endif

            <a href="/books/{{$book->id}}">
                <img width="300" src="{{ $book->image_path }}"/>
                <br/>
                {{ $book->title }}
                <br/>
                @if($book->available)
                <span style="color:green">Available</span>
                @else
                <span style="color:red">Unavailable</span>
                @endif
            </a>
            </td>
            <?php $i++; ?>

            @if($i == 3)
                </tr>
                <?php $i = 0; ?>
            @endif
        @endforeach
</tbody>
</table>
@else
<strong>I did not donate any books to Ketabuk yet. Hopefully I will soon :D</strong>
@endif
</div>
</div>
</div>
</div>
</div>
@endsection
