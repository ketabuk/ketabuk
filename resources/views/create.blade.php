@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header">Donate a book to Ketabuk</div>
<div class="card-body">

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="alert alert-primary" role="alert">
Illegal copies of books are not acceptable on ketabuk.org
If your copy is illegal please don't upload it here.
</div>

<p><span style="color:red">* </span>means that this field is required</p>

<form method="POST" action="/books" enctype="multipart/form-data">
@csrf
<label><span style="color:red">* </span>Book Title:</label>
<br/>
<input name="title" required>
<br/>
<br/>
<label><span style="color:red">* </span>Author: </label>
<br/>
<input name="author" required>
<br/>
<br/>
<label>Translator (if applicable): </label>
<br/>
<input name="translator">
<br/>
<br/>
<label><span style="color:red">* </span>Photo of your copy of the book: </label>
<br/>
<input name="image" type="file" accept="image/*">
<br/>
<br/>
<label><span style="color:red">* </span>Category</label>
<br/>
<select class="custom-select" name="category" required>
@foreach ($categories as $category)
<option value="{{ $category->id }}">{{ $category->name }}</option>
@endforeach
</select>
<br/>
<br/>
<button type="submit" class="btn btn-primary">Submit</button>
</form>

</div>
</div>
</div>
</div>
</div>
@endsection
