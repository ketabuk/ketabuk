@extends('layouts.app')

@section('content')

<?php 
$i = 0;
?>

<div class="container">
<div class="row justify-content-center">
<div class="col-md-12">
<div class="card">
<div class="card-header"><h2>Select a book to transfer to {{ $user->name }}</h2></div>

<div class="card-body">

@if(Auth::user()->books_held->count() > 0)
<table class="table">
<tbody>
        @foreach (Auth::user()->books_held as $book)
            @if($i == 0)
                <tr>
                <th scope="row"></th>
            @endif

            <td>

            @if( $book->image_path == null)
                <?php
                    $r = rand(1, 8);
                    $book->image_path = '/images/no_image' . strval($r) . '.png';
                ?>
            @endif

            <!-- Button trigger modal -->
            <a data-toggle="modal" data-target="#transferConfirm{{$book->id}}">
              <img width="300" src="{{ $book->image_path }}"/>
                <br/>
                {{ $book->title }}
                <br/>
                @if($book->available)
                <span style="color:green">Available</span>
                @else
                <span style="color:red">Unavailable</span>
                @endif
            </a>

            <!-- Modal -->
            <div class="modal fade" id="transferConfirm{{$book->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm Book Transfer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <strong style="color:red">THIS ACTION IS CANNOT BE UNDONE</strong>
                    <p>Are you sure you want to transfer the book: "{{ $book->title }}" to {{ $user->name }}?</p>
                    
                  </div>
                  <div class="modal-footer">
                    <form method="POST" action="/transfer">
                    @csrf
                    <input type="hidden" name="book" value="{{$book->id}}">
                    <input type="hidden" name="user" value="{{$user->id}}">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Transfer</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            </td>
            <?php $i++; ?>

            @if($i == 3)
                </tr>
                <?php $i = 0; ?>
            @endif

        @endforeach
</tbody>
</table>
@else
<strong>You don't currently hold any books.</strong>
<br/>
<br/>
@endif

</div>
</div>
</div>
</div>
</div>

@endsection
