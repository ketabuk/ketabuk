<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Book;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Book::class, function (Faker $faker)
{
	$r = rand(0, 99);
	$image_path = null;
	$translator = null;

	if((bool)rand(0, 1))
	{
		$image_path = Storage::url('book_images/' . strval($r) . '.jpg');
	}
	
	if((bool)rand(0, 1))
	{
		$translator = $faker->name;
	}

    return [
        'title' => $faker->bs,
        'author' => $faker->name,
        'translator' => $translator,
        'image_path' => $image_path,
        'available' => (bool)rand(0, 1)
    ];
});
