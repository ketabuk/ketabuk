<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => "Fiction"
        ]);

        DB::table('categories')->insert([
        	'name' => "Art"
        ]);

        DB::table('categories')->insert([
        	'name' => "Autobiography"
        ]);

        DB::table('categories')->insert([
        	'name' => "Biography"
        ]);

        DB::table('categories')->insert([
        	'name' => "Cookbook"
        ]);

        DB::table('categories')->insert([
        	'name' => "Diary"
        ]);

        DB::table('categories')->insert([
        	'name' => "Dictionary"
        ]);

        DB::table('categories')->insert([
        	'name' => "Encyclopedia"
        ]);

        DB::table('categories')->insert([
        	'name' => "Guide"
        ]);

        DB::table('categories')->insert([
        	'name' => "Health"
        ]);

        DB::table('categories')->insert([
        	'name' => "History"
        ]);

        DB::table('categories')->insert([
        	'name' => "Journal"
        ]);

        DB::table('categories')->insert([
        	'name' => "Math"
        ]);

        DB::table('categories')->insert([
        	'name' => "Memoir"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Religion and Spirituality"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Science"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Travel"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Action and Adventure"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Alternate History"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Children's"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Comic Book"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Crime"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Drama"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Fairytale"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Fantasy"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Graphic Novel"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Historical Fiction"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Horror"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Mystery"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Paranormal Romance"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Picture Book"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Poetry"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Political Thriller"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Romance"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Satire"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Science Fiction "
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Short Story"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Suspense"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Thriller"
        ]);
        
        DB::table('categories')->insert([
        	'name' => "Young Adult"
        ]);   
    }
}
