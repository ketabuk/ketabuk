<?php

use App\User;
use App\Book;
use App\Category;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i = 0; $i < 65; $i++)
    	{ 
    		factory(Book::class)->create([
	        	'original_owner' => User::inRandomOrder()->first()->id,
	        	'current_holder' => User::inRandomOrder()->first()->id,
	        	'category_id' => Category::inRandomOrder()->first()->id
	        ]);
    	}
    }
}
