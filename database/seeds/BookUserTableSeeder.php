<?php

use App\User;
use App\Book;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$books = Book::all();

    	foreach ($books as $book)
    	{
    		if($book->owner->id == $book->holder->id)
    		{
    			continue;
    		}

    		DB::table('book_user')->insert([
        		'book_id' => $book->id,
        		'user_id' => $book->owner->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now()
        	]);

    		for ($i = 0; $i < rand(0, 4); $i++)
    		{ 
    			DB::table('book_user')->insert([
	        		'book_id' => $book->id,
	        		'user_id' => User::inRandomOrder()->first()->id,
	        		'created_at' => Carbon::now(),
        			'updated_at' => Carbon::now()
	        	]);
    		}

        	DB::table('book_user')->insert([
        		'book_id' => $book->id,
        		'user_id' => $book->holder->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now()
        	]);
    	}   
    }
}
