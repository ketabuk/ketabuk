<?php

use App\User;
use App\Message;
use App\Conversation;
use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $users_max_idx = count($users) - 2;

        foreach ($users as $user)
        {
        	$user2_idx = rand(0, $users_max_idx);

        	$user1_id = $user->id;
            $user2 = $users[$user2_idx];
        	$user2_id = $users[$user2_idx]->id;

            if ($user1_id == $user2_id)
            {
                $user2_idx++;
                $user2_id = $users[$user2_idx]->id;
            }

        	$conv = new Conversation;
            $conv->save();
            $conv->users()->saveMany([$user, $user2]);

        	$faker = Faker\Factory::create();
        	$messages = collect([]);

        	for ($i = 0; $i < rand(1, 30); $i++)
        	{ 
        		$m = new Message;
                $m->conversation_id = $conv->id;
        		
        		if((bool)rand(0, 1))
				{
					$m->user_id = $user1_id;
				}
				else
				{
					$m->user_id = $user2_id;
				}
        		
        		$m->body = $faker->realText();
        		$messages->push($m);
        	}

        	$conv->messages()->saveMany($messages);
        }
    }
}