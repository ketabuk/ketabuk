<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('author');
            $table->string('translator')->nullable();
            $table->string('image_path')->nullable();
            $table->boolean('available');
            $table->unsignedBigInteger('original_owner');
            $table->unsignedBigInteger('current_holder');
            $table->foreign('original_owner')->references('id')->on('users');
            $table->foreign('current_holder')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
