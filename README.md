# Ketabuk
~~Will possibly buy ketabuk.org.~~ Did buy ketabuk.org for a year el7amdulellah :D

### 3 Rules:
1. You give away your book. It's not yours anymore. You can't get it back.
1. If you receive a book you do not own it. You must read it then forward it to someone else
1. If you possess a book and you haven't been reading it for 3 weeks you have to forward it to someone else.

### How do we enforce these rules?
I don't. I just hope people will follow. I trust people :)

### Wait, what exactly is this?
A website where people give other people books for free because:
1. They are nice people.
1. They are done reading these books and they won't read them again

And of course they can take other peoples' books to read as well :)

The idea is that books on the shelf are pretty useless. So let's give them away to
other people to enjoy the knowledge and culture hidden on our shelves.

Unlike similar ideas. We don't require this to be an exchange.
You just give away your books.
Or take other peoples' books.

### I want to help!
Great! If the website is up, please donate a book or request one and read it :D

Tell your friends about it. Tell your parents about it. Evangalise so hard about
how this is the truth we've all been looking for. Tell them how blind we've been
all this time. JK be cool, Don't be that guy.

### Help with the development?
I organize the work on this [Trello Board](https://trello.com/b/grxbK9HC/development).

Contact me and I'll assign you a task. Here's my email: adhamzahranfms@gmail.com
